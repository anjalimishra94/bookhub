﻿using System;
using BookHub.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookHub.ViewModels
{
    public class WishListViewModel
    {

        public List<WishListItem> WishListItems { get; set; }
        public WishList WishList { get; set; }

    }
}