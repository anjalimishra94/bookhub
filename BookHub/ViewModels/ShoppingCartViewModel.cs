﻿using BookHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookHub.ViewModels
{
    public class ShoppingCartViewModel
    {
        public List<CartItem> CartItems { get; set; }
        public Cart Cart { get; set; }
        public float CartTotal { get; set; }


    }
}