﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BookHub.Models;

namespace BookHub.ViewModels
{
    public class BuyerAndBookAndSellerViewModel
    {
        
        public IEnumerable<BookAndSellerMapDisplay> BookAndSellerMapDisplays { get; set; }
        public IEnumerable<SliderContent> SliderContents { get; set; }
    }
}