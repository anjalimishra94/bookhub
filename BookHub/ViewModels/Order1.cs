﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BookHub.Models;

namespace BookHub.ViewModels
{
    public class Order1
    {
        public Address Address { get; set; }
        public Cart Cart { get; set; }

        public ICollection<PaymentOption> PaymentOptions { get; set; }

        public int SelectedPayment { get; set; }
    }
}