﻿using BookHub.DAL;
using BookHub.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BookHub.ViewModels;

namespace BookHub.Controllers
{
    public class AdminController : Controller
    {
        MyContextClass db = new MyContextClass();
        //
        // GET: /Admin/
        public ActionResult Index()
        {
            if(Session["Uname"].ToString().Equals(null)||Session["UFname"].ToString().Equals(null))
            {

                return RedirectToAction("Login", "Account");
            }
            else
            { 
                return View(); 
            }
        }

        // GET: /Account/AdminLogin
        public ActionResult SetSlider()
        {
            var list = db.SliderContents.ToList();
            int a = list.Count;
            if(a<5) 
            return View();
            else
            {
                TempData["AlertMessage"] = "The Slider Content Can Have A Maximum of 5 Entries. Delete Some entry to Add a New One.";
                return Redirect(Request.UrlReferrer.ToString());
            }
        }
  [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SetSlider(SliderContent sliderContent, HttpPostedFileBase upload)
        {
            int userId = Convert.ToInt32(Session["Uid"]);
            
            if (ModelState.IsValid)
            {
                if (upload != null && upload.ContentLength > 0)
                {
                    var photo = new SliderData
                    {
                        FileName = System.IO.Path.GetFileName(upload.FileName),
                        FileType = FileType.Photo,
                        ContentType = upload.ContentType
                    };
                    using (var reader = new System.IO.BinaryReader(upload.InputStream))
                    {
                        photo.Content = reader.ReadBytes(upload.ContentLength);
                    }
                    sliderContent.SliderDatas = new List<SliderData> { photo };
                }
                db.SliderContents.Add(sliderContent);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(sliderContent);
        }
        public ActionResult ViewSlider()
        {
            return View(db.SliderContents.ToList());
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SliderContent slidercontent = db.SliderContents.Find(id);
            if (slidercontent == null)
            {
                return HttpNotFound();
            }
            return View(slidercontent);
        }
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SliderContent slidercontent = db.SliderContents.Find(id);
            if (slidercontent == null)
            {
                return HttpNotFound();
            }
            return View(slidercontent);
        }

        // POST: /AdminTemp/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SliderContentId,SliderHeading,SliderSubHeading")] SliderContent slidercontent)
        {
            if (ModelState.IsValid)
            {
                db.Entry(slidercontent).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("ViewSlider");
            }
            return View(slidercontent);
        }

        // GET: /AdminTemp/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SliderContent slidercontent = db.SliderContents.Find(id);
            if (slidercontent == null)
            {
                return HttpNotFound();
            }
            return View(slidercontent);
        }

        // POST: /AdminTemp/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SliderContent slidercontent = db.SliderContents.Find(id);
            db.SliderContents.Remove(slidercontent);
            db.SaveChanges();
            return RedirectToAction("ViewSlider");
        }
            // GET: /Admin/Createlang
        public ActionResult Createlang()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Createlang([Bind(Include="ID,LanguageName,LanguageOrigin")] Language model)
        {
            if (ModelState.IsValid)
            {
                db.Languages.Add(model);
                db.SaveChanges();
                return RedirectToAction("Index","Admin");
            }
                return View(model);
        }
        public ActionResult AddFormat()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddFormat([Bind(Include="ID,BookFormatName")] BookFormat BookFormat)
        {
            if(ModelState.IsValid)
            {
                db.BookFormat.Add(BookFormat);
                db.SaveChanges();
                RedirectToAction("Index", "Admin");
            }
            return View(BookFormat);
        }
        public ActionResult AddType()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddType([Bind(Include="ID,BookTypeName")] BookType BookType)
        {
            if(ModelState.IsValid)
            {
                db.BookTypes.Add(BookType);
                db.SaveChanges();
                RedirectToAction("Index", "Admin");
            }
            return View(BookType);
        }
        // GET: /Admin/Create
        public ActionResult AddBook()
        {
            ViewBag.BookTypeID = new SelectList(db.BookTypes, "BookTypeID", "BookTypeName");
           
            return View();
        }

        // POST: /Admin/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddBook(Book book)
        {
            book.VerificationStatus = 1;
            //book.SellerID = 5;
            book.AvgRating = 0.0f;
            book.eBookUrl = "https://www.google.co.in/";
            book.CreatedOn = DateTime.Now;
            book.TotalBookCount = 0;

            //if (ModelState.IsValid)
            //{               
                db.Books.Add(book);
               
                db.SaveChanges();
                return RedirectToAction("Index","Admin");
            //}
            //return View(book);
        }

      //../Admin/manage user
        //../Admin/Manage Buyer
        public ActionResult manageBuyer()
        {
            var managebuyer = db.Buyers.Include(b => b.User).Select(x => x).ToList();
          
            return View(managebuyer);
        }
        public ActionResult BDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Buyer buyer = db.Buyers.Find(id);
            if (buyer == null)
            {
                return HttpNotFound();
            }
            return View(buyer);
        }
        // GET: /Admin/BuyerDelete
        public ActionResult BDelete(int?id)
        {
            if (id==null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            }
            Buyer buyer = db.Buyers.Find(id);
            if(buyer==null)
            {
                return HttpNotFound();
            }
            return View(buyer);
        }
        //Post: /Admin/BuyerDelete
        [HttpPost,ActionName("BDelete")]
        [ValidateAntiForgeryToken]
        public ActionResult BDeleteConfirmed(int id)
        {
            Buyer buyer = db.Buyers.Find(id);
            db.Buyers.Remove(buyer);
            db.SaveChanges();
           return RedirectToAction("manageBuyer");
        }
        public ActionResult BEdit(int?id)
        {
            if(id==null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Buyer buyer =db.Buyers.Find(id);
            if(buyer==null)
            {
                return HttpNotFound();
            }
            ViewBag.UserID = new SelectList(db.Users, "UserID", "IsActive", buyer.UserID);
          
            return View(buyer);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult BEdit(Buyer buyer)
        {
            var userdata = db.Users.Where(b => b.UserID == buyer.UserID).Select(b => b).FirstOrDefault();
            userdata.isActive = buyer.User.isActive;
           
            db.Entry(userdata).State = EntityState.Modified;
            db.SaveChanges();
          return RedirectToAction("manageBuyer");

        }
        //../Admin/Manage Seller
        public ActionResult manageSeller()
        {
            var manageseller = db.Sellers.Include(b => b.User).Select(b => b).ToList();
            return View(manageseller);
        }
        public ActionResult SDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Seller seller = db.Sellers.Find(id);
            if (seller == null)
            {
                return HttpNotFound();
            }
            return View(seller);
        }
        // GET: /Admin/SellerDelete
        public ActionResult SDelete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Seller seller = db.Sellers.Find(id);
            if (seller == null)
            {
                return HttpNotFound();
            }
            return View(seller);
        }

        // POST: /Admin/SellerDelete 
        [HttpPost, ActionName("SDelete")]
        [ValidateAntiForgeryToken]
        public ActionResult SDeleteConfirmed(int id)
        {
            Seller seller = db.Sellers.Find(id);
            db.Sellers.Remove(seller);
            //db.Users.Where(b => b.UserID == seller.User.UserID).;
            db.SaveChanges();
            return RedirectToAction("manageSeller");
        }
       //Admin/blockuser
        public ActionResult SEdit(int?id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Seller seller = db.Sellers.Find(id);
           if (seller == null)
            {
                return HttpNotFound();
            }
           ViewBag.UserID = new SelectList(db.Users, "UserID", "IsActive", seller.UserID);
          
            return View(seller);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public ActionResult SEdit(Seller seller)
        {
            var userdata = db.Users.Where(x => x.UserID == seller.UserID).Select(x => x).FirstOrDefault();
            userdata.isActive = seller.User.isActive;
            //var userdata = db.Users.Where(x => x.UserID == user.UserID).Select(x => x).FirstOrDefault();
            //userdata.isActive = user.isActive;
            db.Entry(userdata).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("manageSeller");
        }
        public ActionResult ProfileView(Admin admins)
        {
            string sessUname = Session["Uname"].ToString();

            int sess = Convert.ToInt32(Session["UID"]);
            var admin = db.Admins.Where(b => b.User.username == sessUname).FirstOrDefault().AdminID;
            var profile = db.Admins.Where(b => b.AdminID == admin).Select(b => b).Include(b => b.User).FirstOrDefault();
            if (profile == null)
            {
                return HttpNotFound();
            }
            return View(profile);
        }
        public ActionResult EditProfile(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Admin admin = db.Admins.Find(id);
            if (admin == null)
            {
                return HttpNotFound();
            }
          
          
            return View(admin);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditProfile(Admin admins)
        {
            string sessUname = Session["Uname"].ToString();
            int sess = Convert.ToInt32(Session["UID"]);
            var item = db.Admins.Where(b => b.User.username == sessUname).FirstOrDefault().AdminID;
            var admin = db.Admins.Where(b => b.AdminID == item).Select(b => b).FirstOrDefault();
            admin.addr = admins.addr;
            admin.city = admins.city;
            admin.contact = admins.contact;
            admin.country = admins.country;
            admin.fname = admins.fname;
            admin.lname = admins.lname;
            admin.state = admins.state;

           
            if (ModelState.IsValid)
            {
                db.Entry(admin).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("ProfileView");
            }
            return View(admins);
        }

    //all book
        public ActionResult IndexAllBook(string currentFilter,int? page, string sortOrder)
        {
            string sessUname = Session["Uname"].ToString();

            int sess = Convert.ToInt32(Session["UID"]);
            var admin = db.Admins.Where(b => b.User.username == sessUname).FirstOrDefault().AdminID;
            ViewBag.CurrentSort = sortOrder;

            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            ViewBag.PriceSortParm = sortOrder == "AvgRating" ? "AvgRating_desc" : "AvgRating";




          return View(db.Books.Include(b=>b.BookType).ToList());
        }
        //get//editbookstatus
        public ActionResult EditBookStatus(int? id)
        {
            if(id==null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Book book = db.Books.Find(id);
            if(book==null)
            {
            return HttpNotFound();
            }
            ViewBag.BookTypeID= new SelectList(db.BookTypes, "BookTypeID","BookType", book.BookTypeID);
                
            return View(book);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditBookstatus(Book book)
        {
            string sessUname = Session["Uname"].ToString();

            int sess = Convert.ToInt32(Session["UID"]);
            var admin = db.Admins.Where(b => b.User.username == sessUname).FirstOrDefault().AdminID;

            var bookitem = db.Books.Where(b => b.BookID == book.BookID).Select(b => b).FirstOrDefault();
            var bookmap = db.BookAndSellerMapsprop.Where(b => b.Book.BookID==bookitem.BookID).Select(b => b).FirstOrDefault();
            bookitem.VerificationStatus = book.VerificationStatus;
            bookitem.eBookUrl ="http://localhost:49440/Admin/EditBookStatus/6";
         
            bookitem.AvgRating = 0.0f;
   
            bookitem.BookName = book.BookName;
        
            bookitem.CreatedOn = DateTime.Now;
            bookitem.TotalBookCount = book.TotalBookCount;
            db.Entry(bookitem).State = EntityState.Modified;
            db.SaveChanges();
            if (bookitem.VerificationStatus > 0)
            {
               
                return RedirectToAction("Index");
            }
            else
            {
                db.Books.Remove(bookitem);
                db.BookAndSellerMapsprop.Remove(bookmap);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
        }
        public ActionResult DetailsBook(int?id)
        {
            if(id==null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Book book =db.Books.Find(id);
            if(book==null)
            {
                return HttpNotFound();
            }
            return View(book);
        }

        public ActionResult Orderitems()
        
        {
            string sessUname = Session["Uname"].ToString();
            int sess = Convert.ToInt32(Session["UID"]);
            var admin = db.Admins.Where(b => b.User.username == sessUname).FirstOrDefault().AdminID;

            var orderitem = db.OrderItems.Select(b => b).Include(b => b.BookAndSellerMap).ToList();

            return View(orderitem);
        }
        public ActionResult OrderitemStatus(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrderItem orderitem = db.OrderItems.Where(x => x.OrderItemID == id).FirstOrDefault();
            if (orderitem == null)
            {
                return HttpNotFound();
            }

            return View(orderitem);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult OrderitemStatus(OrderItem orderitem,Order order)
        {
            string sessUname = Session["Uname"].ToString();
            int sess = Convert.ToInt32(Session["UID"]);
            var admin = db.Admins.Where(b => b.User.username == sessUname).FirstOrDefault().AdminID; //var seller = db.Sellers.Where(s => s.SellerID == sellerid).Select(s => s).FirstOrDefault();
            //var orderid = db.OrderItems.Where(o => o.BookAndSellerMap.SellerID == sellerid).FirstOrDefault().OrderItemID;
            //var orders = db.OrderItems.Where(o => o.OrderItemID == orderid).Select(b => b);
            //  var order= db.OrderItems.Where(b => b.BookAndSellerMap.SellerID == sellerid).Include(x => x.OrderItemStatus).FirstOrDefault();
           
            var orders = db.OrderItems.Select(x => x).Where(o => o.OrderItemID == orderitem.OrderItemID).FirstOrDefault();
            orders.OrderItemStatus = orderitem.OrderItemStatus;
                    
              //var orderst=orderwhole 
         
                db.Entry(orders).State = EntityState.Modified;
                db.SaveChanges();

                var ordert = db.Orders.Where(b => b.OrderID == orders.OrderID).FirstOrDefault();
                var orderst = db.OrderItems.Where(b => b.OrderID == order.OrderID && b.OrderItemStatus == 0).Select(x => x).ToList();
               if (orderst.Count <= 0)
                {
                   ordert.OrderStatus = 1;
                    db.Entry(ordert).State = EntityState.Modified;
                    db.SaveChanges();

                }

               return RedirectToAction("Index");
        
                  
        }
    }
}

