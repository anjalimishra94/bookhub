﻿using BookHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Mail;
using System.Net;
using BookHub.DAL;
namespace BookHub.Controllers
{
    public class SendMailerController : Controller
    {
        MyContextClass db = new MyContextClass();
   
        //
        // GET: /SendMailer/  
        public ActionResult Index()
        {
            return View();
        } 
        [HttpPost]
        public ViewResult Index(MailModel objmail)
        {

            if (ModelState.IsValid)
            {
                string feedback = "";
                MailMessage mail = new MailMessage();
                mail.To.Add(objmail.To);
                mail.From = new MailAddress("anjali.mishra@compunnel.com");
                string body = objmail.Body;
                mail.Body = body;
                mail.Subject = objmail.Subject;
                mail.CC.Add(objmail.CC);
                mail.Bcc.Add(objmail.BCC);

                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 25;
                //smtp.UseDefaultCredentials = false;
               // smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
               smtp.Credentials = new NetworkCredential 
                ("anjali.mishra@compunnel.com", "anjali1234@" );
                smtp.EnableSsl = true;
                //smtp.Send(mail);
                try
                {
                    smtp.Send(mail);
                    feedback = "Message sent to insurance";
                }
                catch (Exception e)
                {
                    feedback = "Message not sent retry" + e.Message;
                }
                MailModel mail2 = new MailModel();
                mail2.BCC = mail.Bcc.ToString();
                mail2.Body = mail.Body;
                mail2.CC = mail.CC.ToString();
                mail2.From = mail.From.ToString();
                mail2.Subject = mail.Subject;
                mail2.To = mail.To.ToString();

                db.MailModels.Add(mail2);
                db.SaveChanges();
                return View("Index");
            }
            else
            {
                return View();
            }
            
        }
	}
}