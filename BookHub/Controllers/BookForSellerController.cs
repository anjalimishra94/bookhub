﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BookHub.Models;
using BookHub.DAL;
using log4net;
using PagedList;
namespace BookHub.Controllers
{
    public class BookForSellerController : Controller
    {
        private MyContextClass db = new MyContextClass();
        readonly ILog logger = LogManager.GetLogger(typeof(BookForSellerController));
        // GET: /BookForSeller/
        public ActionResult Index(string currentFilter, string searchString,int? page,string sortOrder)
        {
            int userId = Convert.ToInt32(Session["Uid"]);
            var seller = db.Sellers.Where(m => m.UserID.Equals(userId)).FirstOrDefault();
            int sellerId = seller.SellerID;

            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            ViewBag.PriceSortParm = sortOrder == "AvgRating" ? "AvgRating_desc" : "AvgRating";

            if (searchString != null)
            {
                page = 1;

            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.currentFilter = searchString;
            var books = db.Books.Include(b => b.BookType);
                //.Where(b=>b.SellerID.Equals(sellerId));
            switch (sortOrder)
            {
                case "name_desc":
                    books = books.OrderByDescending(b => b.BookName);
                    break;

                case "AvgRating":
                    books = books.OrderBy(b => b.CreatedOn);
                    break;

                case "AvgRating_desc":
                    books = books.OrderByDescending(b => b.BookName);
                    break;
                default:
                    books = books.OrderBy(b => b.CreatedOn);
                    break;
            }

            if (!String.IsNullOrEmpty(searchString))
            {
                books = books.Where(b => b.BookName.Contains(searchString));

            }
            int pageSize = 3;
            int pageNumber = (page ?? 1);
            return View(books.ToPagedList(pageNumber, pageSize));
            //return View(books.ToList());
        }

        public ActionResult Index2()
        {
            var bookandsellermapsprop = db.BookAndSellerMapsprop.Include(b => b.Book).Include(b => b.BookFormat).Include(b => b.Language).Include(b => b.Seller);
            return View(bookandsellermapsprop.ToList());
        }

        // GET: /BookForSeller/Details/5
        public ActionResult Details(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                Book book = db.Books.Find(id);
                String name = book.AuthorName;
                if (book == null)
                {
                    return HttpNotFound();
                }
                return View(book);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return View();
        }
        // GET: /BookForSeller/Create
        public ActionResult Create()
        {
            ViewBag.BookTypeID = new SelectList(db.BookTypes, "BookTypeID", "BookTypeName");
            //ViewBag.SellerID = new SelectList(db.Sellers, "SellerID", "fname");
            return View();
        }

        // POST: /BookForSeller/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Book book)
        {
            book.AvgRating = 0.0f;
            book.eBookUrl = "http://localhost:49440/BookForSeller/Create";

            //int userId = Convert.ToInt32(Session["Uid"]);
            //var seller = db.Sellers.Where(m => m.UserID.Equals(userId)).FirstOrDefault();
            //int sellerId = seller.SellerID;

            //book.SellerID = sellerId;
            book.TotalBookCount = 0;
            book.VerificationStatus = 0;
            book.CreatedOn = DateTime.Now;
      //if (ModelState.IsValid)
        
      //      {
                var existing = db.Books.Where(b => b.BookName==book.BookName && b.AuthorName==book.AuthorName).FirstOrDefault();
                if (existing == null)
                {
                    db.Books.Add(book);
                    db.SaveChanges();
                    return RedirectToAction("SIndex", "Seller");
                }
                else
                {
                    ModelState.AddModelError("", "The Entered Book already exists.");
                   // ViewBag.BookTypeID = new SelectList(db.BookTypes, "BookTypeID", "BookTypeName", book.BookTypeID);
                    return View(book);
                }
        }
        // GET: /BookForSeller/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Book book = db.Books.Find(id);
            if (book == null)
            {
                return HttpNotFound();
            }
            ViewBag.BookTypeID = new SelectList(db.BookTypes, "BookTypeID", "BookTypeName", book.BookTypeID);
            //ViewBag.SellerID = new SelectList(db.Sellers, "SellerID", "fname", book.SellerID);
            return View(book);
        }

        // POST: /BookForSeller/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="BookID,BookName,AuthorName,BookTypeID,BookDescription,AvgRating,TotalBookCount,eBookUrl,VerificationStatus,SellerID")] Book book)
        {
            if (ModelState.IsValid)
            {
                db.Entry(book).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.BookTypeID = new SelectList(db.BookTypes, "BookTypeID", "BookTypeName", book.BookTypeID);
            //ViewBag.SellerID = new SelectList(db.Sellers, "SellerID", "fname", book.SellerID);
            return View(book);
        }

        // GET: /BookForSeller/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Book book = db.Books.Find(id);
            if (book == null)
            {
                return HttpNotFound();
            }
            return View(book);
        }

        // POST: /BookForSeller/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Book book = db.Books.Find(id);
            db.Books.Remove(book);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        //functions to create content for BookAndSellerMap
        public ActionResult Create2()
        {
            ViewBag.BookID = new SelectList(db.Books.Where(b=>b.VerificationStatus.Equals(1)), "BookID", "BookName");
            ViewBag.BookFormatID = new SelectList(db.BookFormat, "BookFormatID", "BookFormatName");
            ViewBag.LanguageID = new SelectList(db.Languages, "LanguageID", "LanguageName");
           // ViewBag.SellerID = new SelectList(db.Sellers, "SellerID", "fname");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create2([Bind(Include = "BookAndSellerMapID,BookID,Publication,PageCount,LanguageID,BookFormatID,Edition,PrintYear,CopiesCount,Price,Discount")] BookAndSellerMap bookandsellermap, HttpPostedFileBase upload)
        {
             int userId = Convert.ToInt32(Session["Uid"]);
            var seller = db.Sellers.Where(m => m.UserID.Equals(userId)).FirstOrDefault();
            int sellerId = seller.SellerID;

            bookandsellermap.FinalPrice = bookandsellermap.Price * (100 - bookandsellermap.Discount) / 100;

            bookandsellermap.SellerID = sellerId;

            bookandsellermap.VerificationStatus = 0;

            bookandsellermap.CreatedOn = DateTime.Now;
     
            if (ModelState.IsValid)
            {
                if (upload != null && upload.ContentLength > 0)
                {
                    var avatar = new File
                    {
                        FileName = System.IO.Path.GetFileName(upload.FileName),
                        FileType = FileType.Avatar,
                        ContentType = upload.ContentType
                    };
                    using (var reader = new System.IO.BinaryReader(upload.InputStream))
                    {
                        avatar.Content = reader.ReadBytes(upload.ContentLength);
                    }
                    bookandsellermap.Files = new List<File> { avatar };
                }

                db.BookAndSellerMapsprop.Add(bookandsellermap);
                db.SaveChanges();

                var more = db.BookAndSellerMapsprop.Where(t => t.BookID.Equals(bookandsellermap.BookID));
                if(more.Count()==1)
                {
                    BookAndSellerMapDisplay obj = new BookAndSellerMapDisplay();
                    obj.BookAndSellerMapId = more.First().BookAndSellerMapID;
                    obj.BookAndSellerMap = more.First();
                    db.BookAndSellerMapDisplays.Add(obj);
                }
                else
                {
                    var old = db.BookAndSellerMapDisplays.Where(t => t.BookAndSellerMap.BookID.Equals(bookandsellermap.BookID)).First() ;

                    if (old.BookAndSellerMap.Price >= bookandsellermap.Price)
                    {
                        var obb = db.BookAndSellerMapsprop.Where(t => t.BookID.Equals(bookandsellermap.BookID)).OrderBy(y => y.Price).First();
                       
                        old.BookAndSellerMapId = obb.BookAndSellerMapID;
                        old.BookAndSellerMap = obb;
                        db.Entry(old).State = EntityState.Modified;
                    }
                }

                db.SaveChanges();
                return RedirectToAction("Index2");
            }

            ViewBag.BookID = new SelectList(db.Books.Where(b => b.VerificationStatus.Equals(1)), "BookID", "BookName", bookandsellermap.BookID);
            ViewBag.BookFormatID = new SelectList(db.BookFormat, "BookFormatID", "BookFormatName", bookandsellermap.BookFormatID);
            ViewBag.LanguageID = new SelectList(db.Languages, "LanguageID", "LanguageName", bookandsellermap.LanguageID);
            //ViewBag.SellerID = new SelectList(db.Sellers, "SellerID", "fname", bookandsellermap.SellerID);
            return View(bookandsellermap);
        }

        public ActionResult Details2(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BookAndSellerMap bookandsellermap = db.BookAndSellerMapsprop.Include(w => w.Book).Include(q => q.Seller).Include(q => q.BookFormat).Include(q => q.Language).Include(s => s.Files).SingleOrDefault(s => s.BookAndSellerMapID == id);
            if (bookandsellermap == null)
            {
                return HttpNotFound();
            }
            return View(bookandsellermap);
        }
    }
}
