﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BookHub.Models;
using BookHub.DAL;

namespace BookHub.Controllers
{
    public class BooksssController : Controller
    {
        private MyContextClass db = new MyContextClass();

        // GET: /Booksss/
        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            var bookandsellermapsprop = db.BookAndSellerMapsprop.Include(b => b.Book).Include(b => b.BookFormat).Include(b => b.Language).Include(b => b.Seller);
            return View(bookandsellermapsprop.ToList());
        }

        // GET: /Booksss/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BookAndSellerMap bookandsellermap = db.BookAndSellerMapsprop.Find(id);
            if (bookandsellermap == null)
            {
                return HttpNotFound();
            }
            return View(bookandsellermap);
        }

        // GET: /Booksss/Create
        public ActionResult Create()
        {
            ViewBag.BookID = new SelectList(db.Books, "BookID", "BookName");
            ViewBag.BookFormatID = new SelectList(db.BookFormat, "BookFormatID", "BookFormatName");
            ViewBag.LanguageID = new SelectList(db.Languages, "LanguageID", "LanguageName");
            ViewBag.SellerID = new SelectList(db.Sellers, "SellerID", "fname");
            return View();
        }

        // POST: /Booksss/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="BookAndSellerMapID,BookID,SellerID,Publication,PageCount,LanguageID,BookFormatID,Edition,PrintYear,CopiesCount,VerificationStatus,Price,Discount")] BookAndSellerMap bookandsellermap)
        {
            if (ModelState.IsValid)
            {
                db.BookAndSellerMapsprop.Add(bookandsellermap);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.BookID = new SelectList(db.Books, "BookID", "BookName", bookandsellermap.BookID);
            ViewBag.BookFormatID = new SelectList(db.BookFormat, "BookFormatID", "BookFormatName", bookandsellermap.BookFormatID);
            ViewBag.LanguageID = new SelectList(db.Languages, "LanguageID", "LanguageName", bookandsellermap.LanguageID);
            ViewBag.SellerID = new SelectList(db.Sellers, "SellerID", "fname", bookandsellermap.SellerID);
            return View(bookandsellermap);
        }

        // GET: /Booksss/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BookAndSellerMap bookandsellermap = db.BookAndSellerMapsprop.Find(id);
            if (bookandsellermap == null)
            {
                return HttpNotFound();
            }
            ViewBag.BookID = new SelectList(db.Books, "BookID", "BookName", bookandsellermap.BookID);
            ViewBag.BookFormatID = new SelectList(db.BookFormat, "BookFormatID", "BookFormatName", bookandsellermap.BookFormatID);
            ViewBag.LanguageID = new SelectList(db.Languages, "LanguageID", "LanguageName", bookandsellermap.LanguageID);
            ViewBag.SellerID = new SelectList(db.Sellers, "SellerID", "fname", bookandsellermap.SellerID);
            return View(bookandsellermap);
        }

        // POST: /Booksss/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="BookAndSellerMapID,BookID,SellerID,Publication,PageCount,LanguageID,BookFormatID,Edition,PrintYear,CopiesCount,VerificationStatus,Price,Discount")] BookAndSellerMap bookandsellermap)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bookandsellermap).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.BookID = new SelectList(db.Books, "BookID", "BookName", bookandsellermap.BookID);
            ViewBag.BookFormatID = new SelectList(db.BookFormat, "BookFormatID", "BookFormatName", bookandsellermap.BookFormatID);
            ViewBag.LanguageID = new SelectList(db.Languages, "LanguageID", "LanguageName", bookandsellermap.LanguageID);
            ViewBag.SellerID = new SelectList(db.Sellers, "SellerID", "fname", bookandsellermap.SellerID);
            return View(bookandsellermap);
        }

        // GET: /Booksss/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BookAndSellerMap bookandsellermap = db.BookAndSellerMapsprop.Find(id);
            if (bookandsellermap == null)
            {
                return HttpNotFound();
            }
            return View(bookandsellermap);
        }

        // POST: /Booksss/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BookAndSellerMap bookandsellermap = db.BookAndSellerMapsprop.Find(id);
            db.BookAndSellerMapsprop.Remove(bookandsellermap);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
