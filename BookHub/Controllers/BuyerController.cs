﻿using BookHub.Models;
using BookHub.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Web;

namespace BookHub.Controllers
{
    public class BuyerController : Controller
    {
        // GET: /Buyer/
       BookHub.DAL.MyContextClass db = new BookHub.DAL.MyContextClass();
        public ActionResult Index()
        {
            BuyerAndBookAndSellerViewModel bb = new BuyerAndBookAndSellerViewModel();
            bb.BookAndSellerMapDisplays = getBookAndSellers();
            bb.SliderContents = getSlider();
            return View(bb);
        }
        public List<SliderContent> getSlider()
        {                 
            var list = db.SliderContents.OrderBy(x => x.SliderContentId).ToList();
            return list;
        }
        public List<BookAndSellerMapDisplay> getBookAndSellers()
        {
            var bookandsellermapsprop = db.BookAndSellerMapDisplays.Include(b => b.BookAndSellerMap.Book).Include(b => b.BookAndSellerMap.BookFormat).Include(b => b.BookAndSellerMap.Language).Include(b => b.BookAndSellerMap.Seller).OrderByDescending(n => n.BookAndSellerMap.CreatedOn);
            return bookandsellermapsprop.ToList();
        }
        public ActionResult Details2(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BookAndSellerMap bookandsellermap = db.BookAndSellerMapsprop.Include(w => w.Book).Include(q => q.Seller).Include(q => q.BookFormat).Include(q => q.Language).Include(s => s.Files).SingleOrDefault(s => s.BookAndSellerMapID == id);
            if (bookandsellermap == null)
            {
                return HttpNotFound();
            }
            return View(bookandsellermap);
        }
        public ActionResult ProfileView(Buyer buyers)
        {
            string sessUname = Session["Uname"].ToString();
           
                int sess = Convert.ToInt32(Session["UID"]);
                var buyer = db.Buyers.Where(b => b.User.username == sessUname).FirstOrDefault().BuyerID;
                var profile = db.Buyers.Where(b => b.BuyerID == buyer).Select(b => b).Include(b=>b.User).FirstOrDefault();
                if (profile == null)
            {
                return HttpNotFound();
            }
                return View(profile);
        }
        public ActionResult EditProfile(int? id)
        {
            if(id==null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Buyer buyer = db.Buyers.Find(id);
            if(buyer==null)
            {
                return HttpNotFound();
            }
            return View(buyer);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditProfile(Buyer buyers)
        {
            string sessUname = Session["Uname"].ToString();
            int sess = Convert.ToInt32(Session["UID"]);
            var item = db.Buyers.Where(b => b.User.username == sessUname).FirstOrDefault().BuyerID;
            var buyer = db.Buyers.Where(b => b.BuyerID == item).Select(b => b).FirstOrDefault();
            buyer.addr = buyers.addr;
            buyer.city = buyers.city;
            buyer.contact=buyers.contact;
            buyer.country=buyers.country;
            buyer.fname = buyers.fname;
            buyer.Landmark = buyers.Landmark;
            buyer.lname = buyers.lname;
            buyer.state = buyers.state;   
   
            if (ModelState.IsValid)
            {
                db.Entry(buyer).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("ProfileView");
            }
            return View(buyers);
        }
        public ActionResult MyOrder(Order order)
        {
            string SUname = Session["Uname"].ToString();
            int sess = Convert.ToInt32(Session["UID"]);
            var buyer = db.Buyers.Where(b => b.UserID==sess).FirstOrDefault().BuyerID;
            var Myorder=db.OrderItems.Where(o=>o.Order.BuyerID==buyer).Select(x=>x).Include(b=>b.BookAndSellerMap).Include(b=>b.BookAndSellerMap.Book).ToList();
            var orders = db.Orders.Where(b => b.BuyerID == buyer).Select(x=>x).FirstOrDefault();
            
            return View(Myorder);
        }
    }
}
