﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BookHub.DAL;
namespace BookHub.Controllers
{
    public class ProfilePicController : Controller
    {
        private MyContextClass db = new MyContextClass();
        //
        // GET: /ProfilePic/
        public ActionResult Index(int id)
        {
            var fileToRetrieve = db.ProfilePics.Find(id);
            return File(fileToRetrieve.Content, fileToRetrieve.ContentType);
          
        }
	}
}