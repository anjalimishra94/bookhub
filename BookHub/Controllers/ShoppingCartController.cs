﻿using BookHub.DAL;
using BookHub.Models;
using BookHub.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace BookHub.Controllers
{
    public class ShoppingCartController : Controller
    {
        MyContextClass db = new MyContextClass();
        //
        // GET: /ShoppingCart/
        public ActionResult Index()
        {
            int sess = Convert.ToInt32(Session["Uid"]);
            int buyerid = db.Buyers.Where(b => b.UserID.Equals(sess)).FirstOrDefault().BuyerID;
            var cart = db.Carts.Where(c => c.BuyerID.Equals(buyerid)).Include(e => e.CartItems).FirstOrDefault();
            ShoppingCartViewModel scvm = new ShoppingCartViewModel();
            scvm.Cart = cart;
            scvm.CartItems = getCartItems(cart);
            scvm.CartTotal = getCartTotal(cart);
            return View(scvm);
        }
        public ActionResult AddToCart(int? id)
        {
            var bookRecToAdd = db.BookAndSellerMapsprop.Find(id);

            int sess = Convert.ToInt32(Session["Uid"]);
            int buyerid = db.Buyers.Where(b => b.UserID.Equals(sess)).FirstOrDefault().BuyerID;
            var cart = db.Carts.Where(c => c.BuyerID.Equals(buyerid)).FirstOrDefault();
            var cartItem = new CartItem();

            if (cart == null)
            {
                cart = new Cart
                {
                    BuyerID = buyerid,
                    Total = 0,
                    CreatedOn = DateTime.Now
                };
                db.Carts.Add(cart);
                db.SaveChanges();
            }

            cartItem = new CartItem
            {
                BookAndSellerMapID = bookRecToAdd.BookAndSellerMapID,
                CartID = cart.CartID,
                Quantity = 0,
                CreatedOn = DateTime.Now
            };
            db.CartItems.Add(cartItem);
            cartItem.Quantity++;
            cart.Total = cart.Total + bookRecToAdd.FinalPrice;
            // db.Entry(cart).State = EntityState.Modified;
            db.SaveChanges();

            TempData["AlertMessage"] = "Book Successfully added to your cart";
            return Redirect(Request.UrlReferrer.ToString());
        }
        public float getCartTotal(Cart cart)
        {
            float tot = cart.Total;
            return tot;
        }
        public List<CartItem> getCartItems(Cart cart)
        {
            var cartItems = db.CartItems.Where(t => t.CartID.Equals(cart.CartID)).Include(q => q.BookAndSellerMap).Include(w => w.BookAndSellerMap.Book);
            return cartItems.ToList();
        }
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            int cartItemID = db.CartItems.Find(id).CartItemID;
            var item1 = db.CartItems.Include(s => s.BookAndSellerMap).Include(w => w.BookAndSellerMap.Book);
            var item = item1.Where(a => a.CartItemID.Equals(cartItemID)).FirstOrDefault();
            if (item == null)
            {
                return HttpNotFound();
            }
            return View(item);
        }

        // POST: /BookForSeller/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CartItem item = db.CartItems.Find(id);
            int sess = Convert.ToInt32(Session["Uid"]);
            int buyerid = db.Buyers.Where(b => b.UserID.Equals(sess)).FirstOrDefault().BuyerID;
            var cart = db.Carts.Where(c => c.BuyerID.Equals(buyerid)).FirstOrDefault();

            cart.CartItems.Remove(item);
            cart.Total -= item.BookAndSellerMap.FinalPrice * item.Quantity;

            db.CartItems.Remove(item);
            db.Entry(cart).State = EntityState.Modified;

            db.SaveChanges();
            return RedirectToAction("Index");
        }
        public ActionResult DeleteAll(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var item = db.Carts.Find(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            return View(item);
        }
        // POST: /BookForSeller/DeleteAll/5
        [HttpPost, ActionName("DeleteAll")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteAllConfirmed(int id)
        {
            Cart cart = db.Carts.Find(id);
            var cartItems = db.CartItems.Where(w => w.CartID.Equals(cart.CartID));

            foreach (var item in cartItems)
            {
                cart.CartItems.Remove(item);
                db.CartItems.Remove(item);
            }
            cart.Total = 0;
            db.Entry(cart).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        public ActionResult CheckoutCart(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var cart = db.Carts.Find(id);
            if (cart == null)
            {
                return HttpNotFound();
            }
            if (cart.Total <= 0)
            {
                ModelState.AddModelError("", "You Cannot Proceed to checkout as your cart value is Zero. Kindly add some items to your cart first.");
            }
            return RedirectToAction("SelectAddress");
        }
        public ActionResult SelectAddress()
        {
            int sess = Convert.ToInt32(Session["Uid"]);
            var buyeridz = db.Buyers.Where(b => b.UserID.Equals(sess)).FirstOrDefault();
            int buyerid = buyeridz.BuyerID;

            var addresses = db.Addresses.Where(a => a.BuyerID.Equals(buyerid)).Include(s => s.Buyer);
            return View(addresses);
        }
        public ActionResult AddAddress()
        {
            return View();
        }
        // POST: /Address/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddAddress([Bind(Include = "AddressID,fname,lname,contact,addr,city,state,country,Landmark")] Address address)
        {
            int sess = Convert.ToInt32(Session["Uid"]);
            var buyeridz = db.Buyers.Where(b => b.UserID.Equals(sess)).FirstOrDefault();
            int buyerid = buyeridz.BuyerID;
            address.BuyerID = buyerid;
            if (ModelState.IsValid)
            {
                db.Addresses.Add(address);
                db.SaveChanges();
                return RedirectToAction("SelectAddress");
            }
            return View(address);
        }

        // GET: /Address/Edit/5
        public ActionResult EditAddress(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Address address = db.Addresses.Find(id);
            if (address == null)
            {
                return HttpNotFound();
            }
            return View(address);
        }
        // POST: /Address/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditAddress([Bind(Include = "AddressID,BuyerID,fname,lname,contact,addr,city,state,country,Landmark")] Address address)
        {
            if (ModelState.IsValid)
            {
                db.Entry(address).State = EntityState.Modified;

                db.SaveChanges();
                return RedirectToAction("SelectAddress");
            }
            return View(address);
        }
        // GET: /Address/Delete/5
        public ActionResult DeleteAddress(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Address address = db.Addresses.Find(id);
            if (address == null)
            {
                return HttpNotFound();
            }
            return View(address);
        }

        // POST: /Address/Delete/5
        [HttpPost, ActionName("DeleteAddress")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteAddressConfirmed(int id)
        {
            Address address = db.Addresses.Find(id);
            db.Addresses.Remove(address);
            db.SaveChanges();
            return RedirectToAction("SelectAddress");
        }
        public ActionResult DeliverHere(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Address address = db.Addresses.Find(id);
            if (address == null)
            {
                return HttpNotFound();
            }
            int sess = Convert.ToInt32(Session["Uid"]);
            int buyerid = db.Buyers.Where(b => b.UserID.Equals(sess)).FirstOrDefault().BuyerID;
            var cartForOrder = db.Carts.Where(c => c.BuyerID.Equals(buyerid)).Include(f => f.CartItems).FirstOrDefault();

            Order1 newOrder = new Order1();
            newOrder.Address = address;
            newOrder.Cart = cartForOrder;
            newOrder.PaymentOptions = db.PaymentOptions.ToList();
            newOrder.SelectedPayment = 0;
            return View(newOrder);
        }
        [ValidateAntiForgeryToken]
        public ActionResult PayNow(Order1 order1)
        {
            return View(order1);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult OrderPlacing(Order1 order1)
        {
            Order newOrder = new Order();
            newOrder.AddressID = order1.Address.AddressID;
            newOrder.Buyer = order1.Address.Buyer;
            newOrder.BuyerID = order1.Address.BuyerID;
            newOrder.PlacedOn = DateTime.Now;
            newOrder.LastUpdated = DateTime.Now;
            newOrder.OrderStatus = 0;      //0 stands for placed
            newOrder.Total = (decimal)order1.Cart.Total;
            newOrder.PaymentOptionID = order1.SelectedPayment;

            db.Orders.Add(newOrder);
            db.SaveChanges();

            Order order3 = db.Orders.Where(o => o.AddressID.Equals(newOrder.AddressID) && o.BuyerID.Equals(newOrder.BuyerID)).FirstOrDefault();
            var itemss = db.CartItems.Where(c => c.CartID.Equals(order1.Cart.CartID)).ToList();
            foreach (var item in itemss)
            {
                OrderItem ir = new OrderItem();

                ir.BookAndSellerMapID = item.BookAndSellerMapID;

                ir.OrderID = order3.OrderID;
                ir.OrderItemStatus = 0;
                ir.Quantity = item.Quantity;

                db.OrderItems.Add(ir);
                order3.OrderItems.Add(ir);
            }

            db.Entry(order3).State = EntityState.Modified;
            db.SaveChanges();
            DeleteAllConfirmed(order1.Cart.CartID);
            return RedirectToAction("Success");
        }
        public ActionResult Success()
        {
            return View();
        }
        //
        //GET: /ShoppingCart/CartSummary
        [ChildActionOnly]
        public ActionResult CartSummary()
        {
            int sess = Convert.ToInt32(Session["Uid"]);
            int buyerid = db.Buyers.Where(b => b.UserID == sess).FirstOrDefault().BuyerID;
            var cart = db.Carts.Where(c => c.BuyerID.Equals(buyerid)).FirstOrDefault();
            //ShoppingCartViewModel scvm = new ShoppingCartViewModel();
            //scvm.Cart = cart;
            //scvm.CartItems = getCartItems(cart);
            var carts = GetCount(cart);

            ViewData["CartCount"] = carts;
            return PartialView("CartSummary");
        }
        public int GetCount(Cart cart)
        {
            int count = 0;
            if (cart != null)
            {
                // Get the count of each item in the cart and sum them up
                count = db.CartItems.Where(x => x.CartID == cart.CartID).Select(x => x).ToList().Count();
            }
            // Return 0 if all entries are null
            return count;
        }
        public ActionResult AddToWish(int? id)
        {
            var addwish = db.CartItems.Where(x => x.CartItemID == id).Select(x => x).FirstOrDefault();
            int sess = Convert.ToInt32(Session["UID"]);
            int buyerid = db.Buyers.Where(b => b.UserID == sess).FirstOrDefault().BuyerID;

            var cart = db.Carts.Where(b => b.BuyerID == buyerid).Include(x => x.CartItems).FirstOrDefault();
            var wish = db.WishLists.Where(b => b.BuyerID == buyerid).Include(x => x.WishListItems).FirstOrDefault();
            var wishitem = db.WishListItems.FirstOrDefault(b => b.WishListID == wish.WishListID);
            //   var wishitem = db.WishListItems.Where(b => b.WishListID.Equals(wish.WishListID) && b.Equals(buyerid)).FirstOrDefault();
            // var cartItem = db.CartItems.FirstOrDefault(c => c.CartID == cart.CartID);
            var wishitems = new WishListItem();
            if (wish == null)//(wishitem == null)
            {
                wish = new WishList
                {
                    BuyerID = buyerid,
                    CreatedOn = DateTime.Now
                };

                db.WishLists.Add(wish);
                db.SaveChanges();
            }
            wishitems = new WishListItem
                  {
                      WishListID = wish.WishListID,
                      BookAndSellerMapID = addwish.BookAndSellerMapID,
                      CreatedOn = DateTime.Now
                  };

            db.WishListItems.Add(wishitems);
            db.SaveChanges();
 //addwish.Quantity = addwish.Quantity - 1;
            cart.Total = cart.Total - addwish.BookAndSellerMap.FinalPrice;
           
            db.CartItems.Remove(addwish);

            
            db.SaveChanges();

            TempData["AlertMessage"] = "Book Successfully added to your wishlist";
            return Redirect(Request.UrlReferrer.ToString());
        }
        //public ActionResult Order(Order)
        //{
        //    string sessUname = Session["Uname"].ToString();
        //    int sess = Convert.ToInt32(Session["UID"]);
        //    var buyer = db.Buyers.Where(b => b.UserID == sess).FirstOrDefault().BuyerID;
        //    var orders = db.Orders.Where(b => b.BuyerID == buyer).Select(x => x).ToList();

        //    return View(orders);
        //}
    }
}