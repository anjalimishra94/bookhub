﻿using BookHub.DAL;
using BookHub.Models;
using BookHub.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
namespace BookHub.Controllers
{
    public class WishListController : Controller
    {
        MyContextClass db = new MyContextClass();
        //
        // GET: /WishList/
        public ActionResult Index()
        {
            int sess = Convert.ToInt32(Session["Uid"]);
            int buyerid = db.Buyers.Where(b => b.UserID.Equals(sess)).FirstOrDefault().BuyerID;
            var wishlist = db.WishLists.Where(w=> w.BuyerID.Equals(buyerid)).Include(w => w.WishListItems).FirstOrDefault();
            if (wishlist == null)
            wishlist = new WishList();
            WishListViewModel wlvm = new WishListViewModel();           
            wlvm.WishList = wishlist;
            wlvm.WishListItems = getWishItems(wishlist);
            return View(wlvm);

        }

        public ActionResult AddToWishList(int? id)
        {
            var bookRecToWish = db.BookAndSellerMapsprop.Find(id);
            int sess = Convert.ToInt32(Session["Uid"]);
            int buyerid = db.Buyers.Where(b => b.UserID.Equals(sess)).FirstOrDefault().BuyerID;
            var wishlist = db.WishLists.Where(c => c.BuyerID.Equals(buyerid)).FirstOrDefault();
            var wishlistItem = new WishListItem();

            if (wishlist == null)
            {
                wishlist = new WishList
                {
                    BuyerID=buyerid,

                    CreatedOn = DateTime.Now
                };
                db.WishLists.Add(wishlist);
                db.SaveChanges();
            }

         wishlistItem = new WishListItem
            {
                BookAndSellerMapID = bookRecToWish.BookAndSellerMapID,
               WishListID=wishlist.WishListID,
                CreatedOn = DateTime.Now
            };
            db.WishListItems.Add(wishlistItem);
         
            // db.Entry(cart).State = EntityState.Modified;
            db.SaveChanges();

            TempData["AlertMessage"] = "Book Successfully added to your cart";
            return Redirect(Request.UrlReferrer.ToString());

        }

        public List<WishListItem> getWishItems(WishList wishlist)
        {
            var wishItems = db.WishListItems.Where(t => t.WishListID==wishlist.WishListID).Include(q => q.BookAndSellerMap).Include(w => w.BookAndSellerMap.Book).ToList();

            return wishItems;
        }
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            int wishItemID = db.WishListItems.Find(id).WishListItemID;
            var item1 = db.WishListItems.Include(s => s.BookAndSellerMap).Include(w => w.BookAndSellerMap.Book);
            var item = item1.Where(a => a.WishListItemID.Equals(wishItemID)).FirstOrDefault();


            if (item == null)
            {
                return HttpNotFound();
            }
            return View(item);
        }

        // POST: /BookForSeller/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            WishListItem item = db.WishListItems.Find(id);


            int sess = Convert.ToInt32(Session["Uid"]);
            int buyerid = db.Buyers.Where(b => b.UserID.Equals(sess)).FirstOrDefault().BuyerID;
            var wishlist = db.WishLists.Where(c => c.BuyerID.Equals(buyerid)).FirstOrDefault();

             wishlist.WishListItems.Remove(item);

            db.WishListItems.Remove(item);
            db.Entry(wishlist).State = EntityState.Modified;

            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult DeleteAll(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var item = db.WishLists.Find(id);


            if (item == null)
            {
                return HttpNotFound();
            }
            return View(item);
        }


        // POST: /BookForSeller/DeleteAll/5
        [HttpPost, ActionName("DeleteAll")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteAllConfirmed(int id)
        {
            WishList wishlist = db.WishLists.Find(id);


            var wishlistItems = db.WishListItems.Where(w => w.WishListID.Equals(wishlist.WishListID));

            foreach (var item in wishlistItems)
            {
                wishlist.WishListItems.Remove(item);
                db.WishListItems.Remove(item);
            }



            db.Entry(wishlist).State = EntityState.Modified;

            db.SaveChanges();
            return RedirectToAction("Index");
        }

   
         public ActionResult AddToCart(int? id)
        {
            var addcart = db.WishListItems.Where(x =>x.WishListItemID == id).Select(x => x).FirstOrDefault();

            int sess = Convert.ToInt32(Session["UID"]);
            int buyerid = db.Buyers.Where(b => b.UserID == sess).FirstOrDefault().BuyerID;
            var cart = db.Carts.Where(b => b.BuyerID == buyerid).Include(x=>x.CartItems).FirstOrDefault();

            var wish = db.WishLists.Where(b => b.BuyerID==buyerid).Include(x=>x.WishListItems).FirstOrDefault();
              //var wishitem = db.WishListItems.Where(b => b.WishListID.Equals(wish.WishListID) && b.Equals(buyerid)).FirstOrDefault();

            if (cart== null)
            {
                cart = new Cart
                {
                    BuyerID=buyerid,
                    CreatedOn=DateTime.Now,
                    Total=0
                    
                };
                db.Carts.Add(cart);
                db.SaveChanges();
            }
                   var cartitems = new CartItem
                  {
                     CartID = cart.CartID,
                      BookAndSellerMapID = addcart.BookAndSellerMapID,
                      CreatedOn = DateTime.Now,
                      Quantity=1
                  };
               cartitems.Quantity++;
            db.CartItems.Add(cartitems);
            db.SaveChanges();
           
            db.WishListItems.Remove(addcart);
            db.SaveChanges();

            TempData["AlertMessage"] = "Book Successfully added";
            return Redirect(Request.UrlReferrer.ToString());

        }
    }
}












         

          












         
      


    
  
       

        
