﻿using BookHub.DAL;
using BookHub.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace BookHub.Controllers
{
    public class SellerController : Controller
    {
        MyContextClass db = new MyContextClass();
        //
        // GET: /Seller/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ProfileView(Seller Sellers)
        {
            string sessUname = Session["Uname"].ToString();

            int sess = Convert.ToInt32(Session["UID"]);
            var seller = db.Sellers.Where(b => b.User.username == sessUname).FirstOrDefault().SellerID;
            var profile = db.Sellers.Where(b => b.SellerID == seller).Select(b => b).Include(b => b.User).FirstOrDefault();
            if (profile == null)
            {
                return HttpNotFound();
            }
            return View(profile);
        }
        public ActionResult EditProfile(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Seller seller = db.Sellers.Find(id);
            if (seller == null)
            {
                return HttpNotFound();
            }
            return View(seller);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditProfile(Seller sellers)
        {
            string sessUname = Session["Uname"].ToString();
            int sess = Convert.ToInt32(Session["UID"]);
            var item = db.Sellers.Where(b => b.User.username == sessUname).FirstOrDefault().SellerID;
            var seller = db.Sellers.Where(b => b.SellerID == item).Select(b => b).FirstOrDefault();
            seller.addr = sellers.addr;
            seller.city = sellers.city;
            seller.contact = sellers.contact;
            seller.country = sellers.country;
            seller.fname = sellers.fname;
            seller.Landmark = sellers.Landmark;
            seller.lname = sellers.lname;
            seller.state = sellers.state;
            seller.RegisteredFirmName = sellers.RegisteredFirmName;
            if (ModelState.IsValid)
            {
                db.Entry(seller).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("ProfileView");
            }
            return View(sellers);
        }
        public ActionResult SOrder()
         
        {
            string sessUname = Session["Uname"].ToString();
            int sess = Convert.ToInt32(Session["UID"]);
            var sellerid = db.Sellers.Where(b => b.User.username == sessUname).FirstOrDefault().SellerID;
            //var seller = db.Sellers.Where(b => b.SellerID == sellerid).FirstOrDefault();
            var myorder= db.OrderItems.Where(b => b.BookAndSellerMap.SellerID == sellerid).Select(b => b).Include(b=>b.BookAndSellerMap);


            return View(myorder);
        }
        public ActionResult OrderStatus(int? id)
        {
           if(id == null)
           {
               return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
           }
           OrderItem orderitem = db.OrderItems.Where(x => x.OrderItemID == id).FirstOrDefault();
           if (orderitem == null)
            {
                return HttpNotFound();
            }
           
           return View(orderitem);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult OrderStatus(OrderItem orderitem)
        {
            string sessUname = Session["Uname"].ToString();
            int sess = Convert.ToInt32(Session["UID"]);
            var sellerid = db.Sellers.Where(b => b.User.username == sessUname).FirstOrDefault().SellerID;
            //var seller = db.Sellers.Where(s => s.SellerID == sellerid).Select(s => s).FirstOrDefault();
            //var orderid = db.OrderItems.Where(o => o.BookAndSellerMap.SellerID == sellerid).FirstOrDefault().OrderItemID;
            //var orders = db.OrderItems.Where(o => o.OrderItemID == orderid).Select(b => b);
          //  var order= db.OrderItems.Where(b => b.BookAndSellerMap.SellerID == sellerid).Include(x => x.OrderItemStatus).FirstOrDefault();
          //  var orders = db.OrderItems.FirstOrDefault(b => b.BookAndSellerMap.SellerID == sellerid);
            var orders = db.OrderItems.Select(x => x).Where(o => o.OrderItemID == orderitem.OrderItemID && o.BookAndSellerMap.SellerID==sellerid).FirstOrDefault();
          
            orders.OrderItemStatus= orderitem.OrderItemStatus;
        
            if(ModelState.IsValid)
            {
                db.Entry(orders).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(orderitem);
        }
	}
}