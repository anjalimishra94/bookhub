﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BookHub.DAL;

namespace BookHub.Controllers
{
    public class SliderDataController : Controller
    {
        private MyContextClass db = new MyContextClass();
        //
        // GET: /File/
        public ActionResult Index(int id)
        {
            var fileToRetrieve = db.SliderDatas.Find(id);
            return File(fileToRetrieve.Content, fileToRetrieve.ContentType);
        }
    }
}