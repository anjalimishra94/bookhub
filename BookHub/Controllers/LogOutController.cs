﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookHub.Controllers
{
    public class LogOutController : Controller
    {
        //
        // GET: /LogOut/
        public ActionResult FinalLogOut()
        {
            return RedirectToAction("Index","Home");
        }
	}
}
