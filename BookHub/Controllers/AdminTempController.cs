﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BookHub.Models;
using BookHub.DAL;

namespace BookHub.Controllers
{
    public class AdminTempController : Controller
    {
        private MyContextClass db = new MyContextClass();

        // GET: /AdminTemp/
        public ActionResult Index()
        {
            return View(db.SliderContents.ToList());
        }

        // GET: /AdminTemp/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SliderContent slidercontent = db.SliderContents.Find(id);
            if (slidercontent == null)
            {
                return HttpNotFound();
            }
            return View(slidercontent);
        }

        // GET: /AdminTemp/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /AdminTemp/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="SliderContentId,SliderHeading,SliderSubHeading")] SliderContent slidercontent)
        {
            if (ModelState.IsValid)
            {
                db.SliderContents.Add(slidercontent);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(slidercontent);
        }

        // GET: /AdminTemp/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SliderContent slidercontent = db.SliderContents.Find(id);
            if (slidercontent == null)
            {
                return HttpNotFound();
            }
            return View(slidercontent);
        }

        // POST: /AdminTemp/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="SliderContentId,SliderHeading,SliderSubHeading")] SliderContent slidercontent)
        {
            if (ModelState.IsValid)
            {
                db.Entry(slidercontent).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(slidercontent);
        }

        // GET: /AdminTemp/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SliderContent slidercontent = db.SliderContents.Find(id);
            if (slidercontent == null)
            {
                return HttpNotFound();
            }
            return View(slidercontent);
        }

        // POST: /AdminTemp/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SliderContent slidercontent = db.SliderContents.Find(id);
            db.SliderContents.Remove(slidercontent);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
