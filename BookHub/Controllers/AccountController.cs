﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using BookHub.Models;
using BookHub.DAL;
using System.Data;
using System.Web.SessionState;
using System.Data.Entity;
using System.Collections;
using System.Net;
using BookHub.ViewModels;
namespace BookHub.Controllers
{
    [SessionState(SessionStateBehavior.Default)]
    [Authorize]
    public class AccountController : Controller
    {
        private MyContextClass db = new MyContextClass();
        //public AccountController()
        //    : this(new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext())))
        //{
        //}
        //public AccountController(UserManager<ApplicationUser> userManager)
        //{
        //    UserManager = userManager;
        //}
        //public UserManager<ApplicationUser> UserManager { get; private set; }
        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ViewAccount()
        {
            var user = db.Users.Include(u => u.Question).Where(u => u.username.Equals((String)Session["Uname"]));
            return View(user);
        }
        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(User model)
        {
            if (model != null)
            {
                var password = new EnCryptDecrypt().EncryptStringAes(model.password);
                //var users = db.Users;
                var user = db.Users.Where(m => m.username.Equals(model.username) && m.password.Equals(password) && m.isActive).FirstOrDefault();
                if (user != null)
                {
                    Session["Uname"] = model.username;
                    Session["Uid"] = user.UserID;
                    // Session["Uid"] = user.First().UserID;
                    // Session["UserOb"] = user.First();

                    if (user.Registered == 0 && user.Role == 1)
                    {
                        return RedirectToAction("RegisBuyer", "Account");
                    }
                    else if (user.Registered == 0 && user.Role == 2)
                    {
                        return RedirectToAction("RegisSeller", "Account");
                    }
                    else if (user.Registered == 0 && user.Role == 3)
                    {
                        return RedirectToAction("RegisAdmin", "Account");
                    }
                    else if (user.Registered == 1)
                    {
                        if (user.Role == 1)
                        {
                            var buyer = db.Buyers.Where(m => m.User.username.Equals(model.username)).FirstOrDefault();
                            Session["UFname"] = buyer.fname;
                            return RedirectToAction("Index", "Buyer");
                        }
                        else if (user.Role == 2)
                        {
                            var seller = db.Sellers.Where(m => m.User.username.Equals(model.username)).FirstOrDefault();
                            Session["UFname"] = seller.fname;
                            return RedirectToAction("Index", "Seller");
                        }
                        else if (user.Role == 3)
                        {
                            var admin = db.Admins.Where(m => m.User.username.Equals(model.username)).FirstOrDefault();
                            Session["UFname"] = admin.fname;
                            return RedirectToAction("Index", "Admin");
                        }
                        return RedirectToAction("Index", "Home");
                    }
                }
                //var users = db.Users.Where(m => m.username.Equals(model.username) && m.password!=password && m.isActive).FirstOrDefault();
                //if (users != null){
               
                //    return RedirectToAction("Login2", "Account");
                //}

                else
                {
                    ModelState.AddModelError("", "Invalid username or password.");
                }
            }
            // If we got this far, something failed, redisplay form
            return View(model);
        }
      
        [AllowAnonymous]
        public ActionResult Create()
        {
            ViewBag.QuestionID = new SelectList(db.Questions, "QuestionID", "Quest");
            return View();
        }

        // POST: /Default1/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "UserID,username,password,QuestionID,securityA,Role")] User model, HttpPostedFileBase upload)
        {
            // try
            //{

            if (ModelState.IsValid)
            {
                var userPass = new EnCryptDecrypt().EncryptStringAes(model.password);
                var user = new User() { username = model.username, password = userPass, QuestionID = model.QuestionID, securityA = model.securityA, Role = model.Role };
                var existing = db.Users.Where(u => u.username.Equals(user.username)).FirstOrDefault();
                if (existing != null)
                {
                    ModelState.AddModelError("", "Unable to register user as an account exists with the same email.");
                    ViewBag.QuestionID = new SelectList(db.Questions, "QuestionID", "Quest", user.QuestionID);
                    return View(model);
                }
                if (upload != null && upload.ContentLength > 0)
                {
                    var profile = new ProfilePic
                    {
                        FileName = System.IO.Path.GetFileName(upload.FileName),
                        FileType = FileType.Avatar,
                        ContentType = upload.ContentType
                    };

                    using (var reader = new System.IO.BinaryReader(upload.InputStream))
                    {
                        profile.Content = reader.ReadBytes(upload.ContentLength);
                    }

                    user.ProfilePics = new List<ProfilePic> { profile };
                }
                db.Users.Add(user);
                db.SaveChanges();
                Session["Uname"] = user.username;
                if (user.Role == 1)
                    return RedirectToAction("RegisBuyer", "Account");
                else if (user.Role == 2)
                    return RedirectToAction("RegisSeller", "Account");
            }
            ViewBag.QuestionID = new SelectList(db.Questions, "QuestionID", "Quest", model.QuestionID);
            return View(model);
            //}
            //catch (DataException)
            //{
            //    ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            //}
            //ViewBag.QuestionID = new SelectList(db.Questions, "QuestionID", "Quest", model.QuestionID);
            //return View(model);
        }
        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        //private async Task SignInAsync(ApplicationUser user, bool isPersistent)
        //{
        //    AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
        //    var identity = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
        //    AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, identity);
        //}

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        //private bool HasPassword()
        //{
        //    var user = UserManager.FindById(User.Identity.GetUserId());
        //    if (user != null)
        //    {
        //        return user.PasswordHash != null;
        //    }
        //    return false;
        //}

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            Error
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        private class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties() { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
        [AllowAnonymous]
        public ActionResult RegisBuyer()
        {
            //ViewBag.AddressID = (new SelectList(db.Addresses, "AddressID", "AddressName")).AsEnumerable();
            //var addre = db.Addresses.ToList();
            //ViewBag.AddressID = new SelectList(addre, "AddressID", "AddressName");
            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult RegisBuyer(Buyer model)
        {
            try
            {
                string sessUname = Session["Uname"].ToString();
                var userID = db.Users.Where(m => m.username.Equals(sessUname)).FirstOrDefault();
                Session["Uid"] = userID.UserID;
                if (ModelState.IsValid && Session["Uname"] != null)
                {
                    var buyer = new Buyer() { UserID = Convert.ToInt32(Session["Uid"]), fname = model.fname, lname = model.lname, contact = model.contact, addr = model.addr, city = model.city, state = model.state, country = model.country, Landmark = model.Landmark, };
                    db.Buyers.Add(buyer);
                    db.SaveChanges();
                    int id = (int)Session["Uid"];
                    User userToUpdate = db.Users.Find(id);
                    userToUpdate.Registered = 1;
                    db.Entry(userToUpdate).State = EntityState.Modified;
                    db.SaveChanges();
                    int sess = Convert.ToInt32(Session["Uid"]);
                    var buyerNow = db.Buyers.Where(b => b.UserID.Equals(sess)).FirstOrDefault();
                    if (buyerNow != null)
                    {
                        int buyerid = buyerNow.BuyerID;
                        var cart = new Cart();
                        cart.BuyerID = buyerid;
                        cart.Total = 0.0f;
                        cart.CreatedOn = DateTime.Now;
                        db.Carts.Add(cart);
                        var wishlist = new WishList();
                        wishlist.BuyerID = buyerid;
                        wishlist.CreatedOn = DateTime.Now;
                        db.WishLists.Add(wishlist); 
                        var address = new Address() { BuyerID = buyerid, city = buyerNow.city, country = buyerNow.country, Landmark = buyerNow.Landmark, contact = buyerNow.contact, fname = buyerNow.fname, lname = buyerNow.lname, state = buyerNow.state, addr = buyerNow.addr, isDefault = true };
                        db.Addresses.Add(address);

                        buyerNow.Addresses.Add(address);
                        db.Entry(buyerNow).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    Session["UFname"] = buyer.fname;
                    return RedirectToAction("Index", "Buyer");
                }
            }
            catch (DataException)
            {
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }
            // If we got this far, something failed, redisplay form
            return View(model);
        }
        [AllowAnonymous]
        public ActionResult RegisSeller()
        {
            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult RegisSeller(Seller model)
        {
            try
            {
                string sessUname = Session["Uname"].ToString();
                var userID = db.Users.Where(m => m.username.Equals(sessUname)).FirstOrDefault();
                Session["Uid"] = userID.UserID;
                if (ModelState.IsValid && Session["Uname"] != null)
                {
                    var seller = new Seller() { UserID = Convert.ToInt32(Session["Uid"]), fname = model.fname, lname = model.lname, contact = model.contact, addr = model.addr, city = model.city, state = model.state, country = model.country, Landmark = model.Landmark, RegisteredFirmName = model.RegisteredFirmName };
                    if (ModelState.IsValid)
                    {
                        db.Sellers.Add(seller);
                        db.SaveChanges();
                        int id = (int)Session["Uid"];
                        User userToUpdate = db.Users.Find(id);
                        userToUpdate.Registered = 1;
                        db.Entry(userToUpdate).State = EntityState.Modified;
                        db.SaveChanges();
                        Session["UFname"] = seller.fname;
                        return RedirectToAction("Index", "Seller");
                    }
                }
            }
            catch (DataException)
            {
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }
            // If we got this far, something failed, redisplay form
            return View(model);
        }
        [AllowAnonymous]
        public ActionResult Settings()
        {
            return View();
        }
        [AllowAnonymous]
        public ActionResult RegisAdmin()
        {
            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult RegisAdmin(Admin model, HttpPostedFileBase upload)
        {
            try
            {
                string sessUname = Session["Uname"].ToString();
                var userID = db.Users.Where(m => m.username.Equals(sessUname)).FirstOrDefault();
                Session["Uid"] = userID.UserID;
                if (ModelState.IsValid && Session["Uname"] != null)
                {
                    var admin = new Admin() { UserID = Convert.ToInt32(Session["Uid"]), fname = model.fname, lname = model.lname, contact = model.contact, addr = model.addr, city = model.city, state = model.state, country = model.country };

                    if (upload != null && upload.ContentLength > 0)
                    {
                        var avatar = new BookHub.Models.AdminPic
                        {
                            FileName = System.IO.Path.GetFileName(upload.FileName),
                            FileType = FileType.Avatar,
                            ContentType = upload.ContentType
                        };
                        using (var reader = new System.IO.BinaryReader(upload.InputStream))
                        {
                            avatar.Content = reader.ReadBytes(upload.ContentLength);
                        }
                        admin.AdminPics = new List<BookHub.Models.AdminPic> { avatar };
                    } 
                    db.Admins.Add(admin);
                    db.SaveChanges();

                    int id = (int)Session["Uid"];
                    User userToUpdate = db.Users.Find(id);
                    userToUpdate.Registered = 1;

                    db.Entry(userToUpdate).State = EntityState.Modified;
                    db.SaveChanges();

                    Session["UFname"] = admin.fname;

                    return RedirectToAction("Index", "Admin");

                }
            }
            catch (System.Data.DataException)
            {
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }
            // If we got this far, something failed, redisplay form
            return View(model);
        }
        [AllowAnonymous]
        public ActionResult CreateAdmin()
        {
            ViewBag.QuestionID = new SelectList(db.Questions, "QuestionID", "Quest");

            return View();
        }

        //
        // POST: /Account/AdminLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult CreateAdmin(User model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var userPass = new EnCryptDecrypt().EncryptStringAes(model.password);
                    var user = new User() { username = model.username, password = userPass, QuestionID = model.QuestionID, securityA = model.securityA, Role = 3 };

                    db.Users.Add(user);
                    db.SaveChanges();
                    Session["Uname"] = user.username;

                    if (user.Role == 3)
                        return RedirectToAction("Index", "Admin");

                    else
                        return View(model);
                }
            }
            catch (System.Data.DataException)
            {
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }
            // If we got this far, something failed, redisplay form
            return View(model);
        }
         [AllowAnonymous]
        public ActionResult ChangePassword()
        { 
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(ManageAccountViewModel ManageAccountViewModel)
        {
            string sessUname = Session["Uname"].ToString();
            int sess = Convert.ToInt32(Session["Uid"]);
            var userdata = db.Users.Where(x => x.UserID == sess).Select(x => x).FirstOrDefault();
            if (ModelState.IsValid)
            {
                if (sessUname != null)
                {

                    var encryptNewPassword = new EnCryptDecrypt().EncryptStringAes(ManageAccountViewModel.OldPassword);
                    if (userdata.password == encryptNewPassword)
                    {
                        userdata.password = new EnCryptDecrypt().EncryptStringAes(ManageAccountViewModel.NewPassword);
                        db.Entry(userdata).State = EntityState.Modified;
                        db.SaveChanges();
                        return RedirectToAction("Success","Account");
                    }
                }
            }
                return View();
        }
          [AllowAnonymous]
        public ActionResult ForgetPassword()
        {
            ViewBag.QuestionID = new SelectList(db.Questions, "QuestionID", "Quest");
            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ForgetPassword(User user)
        {
            if(user!=null)
            {
                var userdata = db.Users.Where(u => u.username == user.username && u.securityA == user.securityA && u.isActive).FirstOrDefault();

                if (userdata != null)
                {
                    Session["Uname"] = user.username;
                    Session["Uid"] = userdata.UserID;
                    // Session["Uid"] = user.First().UserID;
                    // Session["UserOb"] = user.First();

                    if (userdata.Registered == 0 && userdata.Role == 1)
                    {
                        return RedirectToAction("RegisBuyer", "Account");
                    }
                    else if (userdata.Registered == 0 && userdata.Role == 2)
                    {
                        return RedirectToAction("RegisSeller", "Account");
                    }
                    else if (userdata.Registered == 0 && userdata.Role == 3)
                    {
                        return RedirectToAction("RegisAdmin", "Account");
                    }
                    else if (userdata.Registered == 1)
                    {
                        if (userdata.Role == 1)
                        {
                            var buyer = db.Buyers.Where(m => m.User.username.Equals(user.username)).FirstOrDefault();
                            Session["UFname"] = buyer.fname;
                            return RedirectToAction("Index", "Buyer");
                        }
                        else if (userdata.Role == 2)
                        {
                            var seller = db.Sellers.Where(m => m.User.username.Equals(user.username)).FirstOrDefault();
                            Session["UFname"] = seller.fname;
                            return RedirectToAction("Index", "Seller");
                        }
                        else if (userdata.Role == 3)
                        {
                            var admin = db.Admins.Where(m => m.User.username.Equals(user.username)).FirstOrDefault();
                            Session["UFname"] = admin.fname;
                            return RedirectToAction("Index", "Admin");
                        }
                        return RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Invalid username or password.");
                }
            }
            // If we got this far, something failed, redisplay form
            return View(user);
        }
     }
 }

