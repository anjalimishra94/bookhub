﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookHub.Controllers
{
    public class StupidController : Controller
    {
        //
        // GET: /Stupid/
        public ActionResult DoneWithYou1()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            Session["Uname"] = null;
            Session["UFname"] = null;
            Session["Uid"] = null;
            return RedirectToAction("Index", "Home");
        }
	}
}