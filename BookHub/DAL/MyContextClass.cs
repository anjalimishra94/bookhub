﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Data.Entity.ModelConfiguration.Conventions;
using BookHub.Models;
using Microsoft.AspNet.Identity.EntityFramework;

namespace BookHub.DAL
{
    public class MyContextClass : DbContext
    {
        public DbSet<Book> Books { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Buyer> Buyers { get; set; }
        public DbSet<Seller> Sellers { get; set; }
        public DbSet<BookType> BookTypes { get; set; }
        public DbSet<BookFormat> BookFormat { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<ProfilePic> ProfilePics { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<BookAndSellerMap> BookAndSellerMapsprop { get; set; }
        public DbSet<BookAndSellerMapDisplay> BookAndSellerMapDisplays { get; set; }
        public DbSet<Language> Languages { get; set; }
        public DbSet<File> Files { get; set; }
        public DbSet<WishListItem> WishListItems { get; set; }
        public DbSet<WishList> WishLists { get; set; }
        public DbSet<MailModel> MailModels { get; set; }
        public DbSet<Cart> Carts { get; set; }
        public DbSet<CartItem> CartItems { get; set; }
        public DbSet<Order> Orders { get; set; }
       
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<Report> Reports { get; set; }
        public DbSet<PaymentOption> PaymentOptions { get; set; }
        public DbSet<Admin> Admins { get; set; }
        public DbSet<AdminPic> AdminPics { get; set; }

        public DbSet<SliderContent> SliderContents { get; set; }
        public DbSet<SliderData> SliderDatas { get; set; }


        public MyContextClass()
            : base("MyContextClass")
        {
           // this.Configuration.LazyLoadingEnabled = false;
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        public System.Data.Entity.DbSet<BookHub.Models.Contact> Contacts { get; set; }
    }
}

