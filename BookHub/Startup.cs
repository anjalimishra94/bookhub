﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BookHub.Startup))]
namespace BookHub
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
