﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookHub.Models
{
    public class PaymentOption
    {
        public int PaymentOptionID { get; set; }
        public string PaymentOptionName { get; set; }
    }
}