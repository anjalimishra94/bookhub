﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace BookHub.Models
{
    public class Admin
    {

        public int AdminID { get; set; }
        public int UserID { get; set; }
        public User User { get; set; }
        [Required]
        public string fname { get; set; }


        [Required]
        public string lname { get; set; }
        [Required]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid Phone number")]
        public string contact { get; set; }
        [Required]
        [Display(Name = "Address")]
        public string addr { get; set; }
        [Required]
        public string city { get; set; }
        [Required]
        public string state { get; set; }
        [Required]
        public string country { get; set; }


        public virtual ICollection<AdminPic> AdminPics { get; set; }
        public IEnumerable<Book> Books { get; set; }
    }
}