﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookHub.Models
{
    public class SliderContent
    {
        public int SliderContentId { get; set; }
        public String SliderHeading { get; set; }
        public String SliderSubHeading { get; set; }


        public virtual ICollection<SliderData> SliderDatas { get; set; }


    }
}