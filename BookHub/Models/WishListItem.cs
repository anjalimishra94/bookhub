﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using System.Web;

namespace BookHub.Models
{
    public class WishListItem
    {
        public int WishListItemID { get; set; }
        public int WishListID { get; set; }
        public virtual WishList WishList { get; set; }
        
        public int BookAndSellerMapID { get; set; }
        public virtual BookAndSellerMap BookAndSellerMap { get; set; }


        [DataType(DataType.Date)]
        public DateTime CreatedOn { get; set; }

    


      
       
    }
}