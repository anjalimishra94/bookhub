﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookHub.Models
{
    public class BookAndSellerMapDisplay
    {
        public int BookAndSellerMapDisplayID { get; set; }

        public virtual BookAndSellerMap BookAndSellerMap { get; set; }
        public int BookAndSellerMapId { get; set; }
    }
}