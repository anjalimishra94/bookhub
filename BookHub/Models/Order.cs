﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BookHub.Models
{
    public class Order
    {
        public int OrderID { get; set; }

        public int BuyerID { get; set; }
        public virtual Buyer Buyer { get; set; }

        public virtual Address Address { get; set; }
        public int AddressID { get; set; }

        public decimal Total { get; set; }


        public int OrderStatus { get; set; }

        [DataType(DataType.Date)]
        public DateTime PlacedOn { get; set; }

        [DataType(DataType.Date)]
       
        public DateTime LastUpdated { get; set; }

        public PaymentOption PaymentOption { get; set; }
        public int PaymentOptionID { get; set; }

        public virtual ICollection<OrderItem> OrderItems { get; set; }

    }
}