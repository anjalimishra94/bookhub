﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BookHub.Models
{
    public class Seller
    {
        public int SellerID { get; set; }
        public int UserID { get; set; }
        public User User { get; set; }
         [Required]
        public string fname { get; set; }
         [Required]
        public string lname { get; set; }
         [Required]
        [DataType(DataType.PhoneNumber)]
        public string contact { get; set; }
         [Required]
        [Display(Name = "Address")]
        public string addr { get; set; }
         [Required]
        public string city { get; set; }
         [Required]
        public string state { get; set; }
         [Required]
        public string country { get; set; }
         [Required]
        public string Landmark { get; set; }
         [Required]
        public string RegisteredFirmName { get; set; }
    }
}