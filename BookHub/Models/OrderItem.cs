﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookHub.Models
{
    public class OrderItem
    {
        public int OrderItemID { get; set; }
        public int OrderID { get; set; }
        public virtual Order Order { get; set; }
        public int BookAndSellerMapID { get; set; }
        public BookAndSellerMap BookAndSellerMap { get; set; }
        public int Quantity { get; set; }
        public int OrderItemStatus { get; set; }
    }
}