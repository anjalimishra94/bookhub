﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BookHub.Models
{
    public class CartItem
    {
        public int CartItemID { get; set; }

        [ForeignKey("Cart")]
        public int CartID { get; set; }
        public virtual Cart Cart { get; set; }


        public int Quantity { get; set; }



        [DataType(DataType.DateTime)]
        public DateTime CreatedOn { get; set; }

        public int BookAndSellerMapID { get; set; }
        public virtual BookAndSellerMap BookAndSellerMap { get; set; }

    }
}