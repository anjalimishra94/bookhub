﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;


namespace BookHub.Models
{
    public class SliderData
    {
        public int SliderDataId { get; set; }
        [StringLength(255)]
        public string FileName { get; set; }
        [StringLength(100)]
        public string ContentType { get; set; }
        public byte[] Content { get; set; }
        public FileType FileType { get; set; }   //must be set to 2

        public int SliderContentId{ get; set; }
        public virtual SliderContent SliderContent { get; set; }
    }
}