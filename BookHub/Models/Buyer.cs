﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BookHub.Models
{
    public class Buyer
    {
        public int BuyerID { get; set; }
        public int UserID { get; set; }
        public User User { get; set; }

         [Required(ErrorMessage = "Your must provide a First Name")]
        [Display(Name="First Name")]
        public string fname { get; set; }

         [Required(ErrorMessage = "Your must provide a Last Name")]
         [Display(Name = "Last Name")]
        public string lname { get; set; }
       // public string gender { get; set; }

        [Required(ErrorMessage = "You must provide a PhoneNumber")]
        [Display(Name = "Contact Number")]
        [DataType(DataType.PhoneNumber)]
       [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid Phone number")]
        public string contact { get; set; }

        [Display(Name="Address")]
        public string addr { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string country { get; set; }
        public string Landmark { get; set; }
        public ICollection<Address> Addresses { get; set; }
   

    }
}