﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

using System.Configuration;
using System.Data.SqlClient;

using System.ComponentModel.DataAnnotations.Schema;

namespace BookHub.Models
{
    public class Book
    {
        public int BookID { get; set; }

        [Required]
        public string BookName { get; set; }
   
        [Required]
        public string AuthorName { get; set; }


        //public System.Collections.Generic.List<BookType> BookTList { get; set; }
        //[Required]
        public int BookTypeID { get; set; }
        public BookType BookType { get; set; }

        [Required]
        public string BookDescription { get; set; }
        public float AvgRating { get; set; }

        public int TotalBookCount { get; set; }

        [Required]
        [DataType(DataType.Url)]
        public string eBookUrl { get; set; }

        public int VerificationStatus { get; set; }

        //this has to be the seller who introduced this book
     

        [DataType(DataType.DateTime)]
        public DateTime CreatedOn { get; set; }
    }
}