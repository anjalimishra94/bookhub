﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BookHub.Models
{
    public class Cart
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CartID { get; set; }

        public int BuyerID { get; set; }
        public Buyer Buyer { get; set; }

        public float Total { get; set; }
        public List<CartItem> CartItems { get; set; }

        [DataType(DataType.Date)]
        public DateTime CreatedOn { get; set; }


    }
}