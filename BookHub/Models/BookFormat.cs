﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BookHub.Models
{
    public class BookFormat
    {
        public int BookFormatID { get; set; }
        [Required]
        public string BookFormatName { get; set; }
    }
}