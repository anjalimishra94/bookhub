﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BookHub.Models
{
    public class User
    {
        public int UserID { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Username (Email) ")]
        [RegularExpression("^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$", ErrorMessage = "Not A Valid Username. Enter a valid Email Address.")]
        public string username { get; set; }

        [Required]
        public string password { get; set; }

        [Display(Name = "Security Question")]
        public int QuestionID { get; set; }
        public Question Question { get; set; }



        [Required]
        [MaxLength(20)]
        public string securityA { get; set; }

        [Required]
        public int Role { get; set; }

        public int Registered { get; set; }
        public Boolean isActive { get; set; }

        public virtual ICollection<ProfilePic> ProfilePics { get; set; }

        public User()
        {
            isActive = true;

        }

    }
}