﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BookHub.Models
{
    public class Language
    {
        public int LanguageID { get; set; }
        public string LanguageName { get; set; }
        public string LanguageOrigin { get; set; }
    }
}
