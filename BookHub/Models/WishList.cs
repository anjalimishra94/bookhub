﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BookHub.Models
{
    public class WishList
    {
        public int WishListID { get; set; }
        public int BuyerID { get; set; }
        public Buyer Buyer { get; set; }
        
        public ICollection<WishListItem> WishListItems{ get; set; }


        [DataType(DataType.Date)]
        public DateTime CreatedOn { get; set; }

    

    }
}