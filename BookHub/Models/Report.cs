﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookHub.Models
{
    public class Report
    {
        public int ReportID { get; set; }
        public int BuyerID { get; set; }
        public virtual Buyer Buyer { get; set; }
        public int BookAndSellerMapID { get; set; }
        public virtual BookAndSellerMap BookAndSellerMap { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }

    }
}