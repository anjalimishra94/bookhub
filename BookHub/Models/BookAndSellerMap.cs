﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace BookHub.Models
{
    public class BookAndSellerMap
    {
        public int BookAndSellerMapID { get; set; }
        public int BookID { get; set; }
        public Book Book { get; set; }
        public int SellerID { get; set; }
        public Seller Seller { get; set; }

        [Required]
        public string Publication { get; set; }

        [Required]
        public int PageCount { get; set; }

        
        public int LanguageID { get; set; }
        public Language Language { get; set; }
        public int BookFormatID { get; set; }
        public BookFormat BookFormat { get; set; }

        [Required]
        public string Edition { get; set; }

        [Required]
        public int PrintYear { get; set; }

        [Required]
        public int CopiesCount { get; set; }

        [Required]
        public int VerificationStatus { get; set; }

        [Required]
        public float Price { get; set; }

        [Required]
        public int Discount { get; set; }
        public DateTime CreatedOn { get; set; }

        [Required]
        public float FinalPrice { get; set; }
         
        public virtual ICollection<File> Files { get; set; }
    }
}