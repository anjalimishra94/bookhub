﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace BookHub.Models
{
    public class BookType
    {
        public int BookTypeID { get; set; }

        [Display(Name="Genre")]
        public string BookTypeName { get; set; }
    
    }
}
